package com.rambo.sharding;

import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.apache.shardingsphere.api.config.sharding.KeyGeneratorConfiguration;
import org.apache.shardingsphere.api.config.sharding.ShardingRuleConfiguration;
import org.apache.shardingsphere.api.config.sharding.TableRuleConfiguration;
import org.apache.shardingsphere.api.config.sharding.strategy.HintShardingStrategyConfiguration;
import org.apache.shardingsphere.api.config.sharding.strategy.InlineShardingStrategyConfiguration;
import org.apache.shardingsphere.api.config.sharding.strategy.StandardShardingStrategyConfiguration;
import org.apache.shardingsphere.api.sharding.hint.HintShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.hint.HintShardingValue;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;
import org.apache.shardingsphere.shardingjdbc.api.ShardingDataSourceFactory;
import org.springframework.boot.SpringApplication;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
@Slf4j
public class ShardingSourceDemo {
    /*public static void main(String[] args) throws SQLException {
        Map<String, DataSource> dataSourceMap  = new ConcurrentHashMap<String, DataSource>();
        HikariDataSource dataSource0 = new HikariDataSource();
        dataSource0.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource0.setJdbcUrl("jdbc:mysql://47.94.232.136:3306/shop_ds_0?serverTimezone=UTC&useSSL=false&useUnicode=true&characterEncoding=UTF-8");
        dataSource0.setUsername("root");
        dataSource0.setPassword("123456");
        dataSourceMap.put("ds0",dataSource0);

        HikariDataSource dataSource1 = new HikariDataSource();
        dataSource1.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource1.setJdbcUrl("jdbc:mysql://47.94.232.136:3306/shop_ds_1?serverTimezone=UTC&useSSL=false&useUnicode=true&characterEncoding=UTF-8");
        dataSource1.setUsername("root");
        dataSource1.setPassword("123456");
        dataSourceMap.put("ds1",dataSource1);


        *//**
         * 分片表组装
         *//*
        //配置分片规则
        ShardingRuleConfiguration shardingRuleConfig = new ShardingRuleConfiguration();
        shardingRuleConfig.getTableRuleConfigs().add(getOrderTableRuleConfiguration());
        shardingRuleConfig.getTableRuleConfigs().add(getOrderItemTableRuleConfiguration());

        //绑定表
        shardingRuleConfig.getBindingTableGroups().add("t_order_item,t_order");

        *//**
         * 分片具体规则
         *//*

        //分库策略，通过"user_id"分库
        shardingRuleConfig.setDefaultDatabaseShardingStrategyConfig(
                new InlineShardingStrategyConfiguration("user_id", "ds$->{user_id % 2}"));


//        shardingRuleConfig.setDefaultDatabaseShardingStrategyConfig(
//                new HintShardingStrategyConfiguration(new HintShardingAlgorithm<Integer>() {
//                    @Override
//                    public Collection<String> doSharding(Collection<String> availableTargetNames, HintShardingValue<Integer> shardingValue) {
//                        List<String> shardingList = new LinkedList<>();
//                        for (String each : availableTargetNames){
//                            for (Integer val : shardingValue.getValues()){
//                                if(each.endsWith(val + "")){
//                                    shardingList.add(each);
//                                }
//                                log.info("availableTargetNames:"+each+":shardingValue:"+val);
//                            }
//                        }
//                        return shardingList;
//                    }
//                }));

        //分表策略，通过"order_id"分表
        shardingRuleConfig.setDefaultTableShardingStrategyConfig(new StandardShardingStrategyConfiguration("order_id",
                new PreciseShardingAlgorithm<Long>() {

                    @Override
                    public String doSharding(Collection<String> collection, PreciseShardingValue<Long> preciseShardingValue) {
                        for (String each : collection) {
                            if (each.endsWith(preciseShardingValue.getValue()%2+"")){
                                return each;
                            }
                        }
                        throw new UnsupportedOperationException();
                    }
                }));


        //获取数据源对象
        Properties properties = new Properties();
        properties.setProperty("sql.show", "true");
        DataSource dataSource = ShardingDataSourceFactory.createDataSource(dataSourceMap, shardingRuleConfig, properties);

        ShardingSourceDemo test = new ShardingSourceDemo();
//        test.insertData(dataSource);
//        test.drop(dataSource);
//        test.delete(dataSource);

        test.select2(dataSource);
    }



    public void insertData(DataSource dataSource) throws SQLException {
        for (int i = 1; i < 10; i++) {
            long orderId = executeAndGetGeneratedKey(dataSource, "INSERT INTO t_order (user_id, address_id, status) VALUES (10,10, 'INIT')");
            execute(dataSource, String.format("INSERT INTO t_order_item (order_id, user_id, status) VALUES (%d, 10, 'INIT')", orderId));
            orderId = executeAndGetGeneratedKey(dataSource, "INSERT INTO t_order (user_id, address_id, status) VALUES (11,11, 'INIT')");
            execute(dataSource, String.format("INSERT INTO t_order_item (order_id, user_id, status) VALUES (%d, 11, 'INIT')", orderId));
        }
    }

    public void drop(DataSource dataSource) throws SQLException {
        execute(dataSource, "DROP TABLE IF EXISTS t_order");
        execute(dataSource, "DROP TABLE IF EXISTS t_order_item;");
    }

    public void delete(DataSource dataSource) throws SQLException {
        execute(dataSource, "delete from t_order");
        execute(dataSource, "delete from t_order_item;");
    }

    public void select2(DataSource dataSource) throws SQLException {
        execute(dataSource,String.format("SELECT i.* FROM t_order o JOIN t_order_item i ON o.order_id=i.order_id WHERE o.order_id in (%d, %d)",405466009580318721l,405466010490482688l));
    }



    private long executeAndGetGeneratedKey(final DataSource dataSource, final String sql) throws SQLException {
        long result = -1;
        try (
                Connection conn = dataSource.getConnection();
                Statement statement = conn.createStatement()) {
            statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                result = resultSet.getLong(1);
            }
        }
        return result;
    }

    private void execute(final DataSource dataSource, final String sql) throws SQLException {
        try (
                Connection conn = dataSource.getConnection();
                Statement statement = conn.createStatement()) {
            statement.execute(sql);
        }
    }


    private static TableRuleConfiguration getOrderItemTableRuleConfiguration() {
        TableRuleConfiguration orderItemTableRuleConfig = new TableRuleConfiguration("t_order_item",
                "ds${0..1}.t_order_item_${[0, 1]}");
        orderItemTableRuleConfig.setKeyGeneratorConfig(new KeyGeneratorConfiguration("SNOWFLAKE"
                , "order_id",getProps()));
        //uuid,snowflake,
        return orderItemTableRuleConfig;
    }

    private static TableRuleConfiguration getOrderTableRuleConfiguration() {
        TableRuleConfiguration orderTableRuleConfig = new TableRuleConfiguration("t_order",
                "ds${0..1}.t_order_${[0, 1]}");
        orderTableRuleConfig.setKeyGeneratorConfig(new KeyGeneratorConfiguration("SNOWFLAKE"
                , "order_id",getProps()));
        return orderTableRuleConfig;
    }


    //雪花算法，需要有机器序号，手动配置序号
    private static Properties getProps(){
        Properties props = new Properties();
        props.setProperty("worker.id", "123");
        return props;
    }*/
}
